﻿using AutoMapper;
using MSDPortal.Application;
using MSDPortal.Application.Mappers;
using MSDPortal.Data.Context;
using MSDPortal.Data.Repository;
using MSDPortal.Domain.Interfaces;
using MSDPortal.Domain.Interfaces.Repository;
using Ninject;

namespace MSDPortal.CrossCouting.IOC
{
    public static class IocConfig
    {
        public static void ConfigureDependencies(IKernel kernel)
        {
            kernel.Bind<IProdutoRepository>().To<ProdutoRepository>();
            kernel.Bind<ProdutoService>().ToSelf();
            kernel.Bind<IClienteRepository>().To<ClienteRepository>();
            kernel.Bind<ClienteService>().ToSelf();


            kernel.Bind<IUnitofWork>().To<UnitofWork>();
            kernel.Bind<IMapper>().ToMethod(ctx =>
                 new Mapper(AutoMapperConfig.CreateConfiguration(), type => ctx.Kernel.Get(type)));
        }
    }
}
