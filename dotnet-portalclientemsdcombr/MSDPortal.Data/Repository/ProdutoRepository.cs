﻿using MSDPortal.Application.Entidades;
using MSDPortal.Data.Context;
using MSDPortal.Domain.Interfaces.Repository;

namespace MSDPortal.Data.Repository
{
    public class ProdutoRepository : BaseRepository<Produto>, IProdutoRepository
    {
        public ProdutoRepository(MSDPortalContext context) : base(context)
        {
        }
    }
}
