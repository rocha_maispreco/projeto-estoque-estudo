﻿using MSDPortal.Application;
using MSDPortal.Domain.Interfaces.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MSDPortal.Presentation.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IProdutoRepository _produto;
        private readonly ProdutoService _produtoService;
        private readonly IClienteRepository _cliente;
        private readonly ClienteService _clienteService;



        public HomeController(IProdutoRepository produto , ProdutoService produtoService)
        {
            _produto = produto;
            _produtoService = produtoService;
        }

        public ActionResult Index()
        {
            _produto.GetAll();
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }

}