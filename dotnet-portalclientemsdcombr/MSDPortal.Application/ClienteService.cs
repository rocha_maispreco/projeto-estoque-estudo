﻿using AutoMapper;
using MSDPortal.Application.Entidades;
using MSDPortal.Domain.Interfaces;
using MSDPortal.Domain.Interfaces.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MSDPortal.Application
{
    public class ClienteService
    {
        private readonly IUnitofWork _unitofWork;
        private readonly IMapper _mapper;
        private readonly IClienteRepository _clienteRepository;

        public ClienteService(IUnitofWork unitofWork, IClienteRepository clienteRepository)
        {
            _unitofWork = unitofWork;
            _clienteRepository = clienteRepository;
        }

        public async Task<Cliente> Add(Cliente model)
        {
            try
            {
                await _unitofWork.ClienteRepository.Add(model);
            }
            catch (Exception ex)
            {
                var error = ex.Message;
                throw ex;
            }

            return model;
        }

        public void Delete(Cliente model)
        {
            try
            {

                _unitofWork.ClienteRepository.Remove(model);
            }
            catch (Exception ex)
            {
                var error = ex.Message;
                throw ex;
            }
        }

        public async Task<Cliente> Get(int id)
        {
            try
            {
                Cliente model = await _unitofWork.ClienteRepository.GetById(id);
                return model;
            }
            catch (Exception ex)
            {
                var error = ex.Message;
                throw ex;
            }
        }

        public async Task<List<Cliente>> GetAll()
        {

            try
            {
                IEnumerable<Cliente> result = await _unitofWork.ClienteRepository.GetAllAsync();
                return result.ToList();
            }
            catch (Exception ex)
            {
                var error = ex.Message;
                throw ex;
            }
        }

        public async Task<Cliente> Update(Cliente model)
        {
            try
            {
                Cliente result = await _unitofWork.ClienteRepository.Update(model);
                return result;

            }
            catch (Exception ex)
            {
                var error = ex.Message;
                throw ex;
            }

        }
    }
}
