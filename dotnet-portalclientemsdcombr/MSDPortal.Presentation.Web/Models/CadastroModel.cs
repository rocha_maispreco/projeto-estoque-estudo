﻿//using MSDPortal.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MSDPortal.Presentation.Web.Modelsdada
{
	public class CadastroModel
	{
        [Display(Name = "Razão Social")]
        public string CompanyName { get; set; }

        [Display(Name = "E-Mail")]
        [EmailAddress(ErrorMessage = "O campo Email não contém um endereço de email válido")]
        public string Email { get; set; }

        [Display(Name = "Nome Fantasia")]
        public string TradeName { get; set; }

        [Display(Name = "CNPJ")]
        public string ENI { get; set; }

        public string Telephones { get; set; }

        public string SubSidiaries { get; set; }

        [Display(Name = "Logo Tipo")]
        public string Logo { get; set; }

        public string CNPJ { get; set; }

        [Display(Name = "Telefone")]
        public string Telephone { get; set; }

        public string Name { get; set; }
        public string CEP { get; set; }
        public string Bairro { get; set; }
        public string Cidade { get; set; }
        public string Estado { get; set; }

        [Display(Name = "Endereço")]
        public string Endereco { get; set; }
        [Display(Name = "Número")]
        public int Number { get; set; }
        public string Description { get; set; }
        public string Complement { get; set; }

        //public string FileLogoStream { get; set; }
    }
}