﻿using MSDPortal.Data.Repository;
using MSDPortal.Domain.Interfaces;
using MSDPortal.Domain.Interfaces.Repository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSDPortal.Data.Context
{
    public class UnitofWork : IUnitofWork
    {
        private MSDPortalContext _context = new MSDPortalContext();

     
        private IProdutoRepository _produtoRepository;
        private IClienteRepository _clienteRepository;
        private bool disposed = false;


        public object Context
        {
            get
            {
                return this._context;
            }
        }

        public MSDPortalContext Create()
        {
            return _context;
        }


    
        public IProdutoRepository ProdutoRepository
        {
            get
            {
                if (this._produtoRepository == null)
                {
                    this._produtoRepository = new ProdutoRepository(_context);
                }

                return _produtoRepository;
            }
        }
        public IClienteRepository ClienteRepository
        {
            get
            {
                if (this._clienteRepository == null)
                {
                    this._clienteRepository = new ClienteRepository(_context);
                }

                return _clienteRepository;
            }
        }

        public async void Save()
        {
             await _context.SaveChangesAsync();
        }        

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        void IUnitofWork.Dispose(bool disposing)
        {
            throw new NotImplementedException();
        }


    }
}
