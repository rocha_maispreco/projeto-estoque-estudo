﻿using MSDPortal.Domain.Entidades;
using System;

namespace MSDPortal.Application.Entidades
{
    public class Produto : BaseEntity
    {
        public string Nome { get; set; }
        public string Marca { get; set; }
        public decimal Preço { get; set; }

        public decimal Juros = 0.25m;

        public decimal ValorFinal()
        {
            return Preço*Juros;
        }
    }
}