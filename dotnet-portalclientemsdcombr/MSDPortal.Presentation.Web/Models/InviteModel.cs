﻿using MSDPortal.Domain.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MSDPortal.Presentation.Web.Modelssadada
{
	public class InviteModel
	{
		[Display(Name = "Email")]
		[EmailAddress(ErrorMessage = "O campo Email não contém um endereço de email válido")]
		[Required(ErrorMessage = "{0} é obrigatório", AllowEmptyStrings = false)]
		public string Email { get; set; }
		//public PerfilEnum Perfil { get; set; }
	}
}