﻿
canvas.renderAll();

fabric.Image.fromURL(this, function (myImg) {
    var img1 = myImg.set({ left: 0, top: 0, width: 150, height: 150 });
    canvas.add(img1);
});

canvas.on('mouse:down', function (event) {
    if (canvas.getActiveObject()) {
        alert(event.target);
    }

});
 
var imgElement = "";
var inputforupload = "";
var readerobj = "";




var canvasObj = new fabric.Canvas(document.getElementById(`${myImg}`), {
    backgroundColor: '#c8c8c8'
});
 

var HideControls = {
    'tl': true,
    'tr': true,
    'bl': true,
    'br': false,
    'ml': false,
    'mt': false,
    'mr': false,
    'mb': false,
    'mtr': false
};

var ctrlImages = new Array()

function preload() {
    for (i = 0; i < preload.arguments.length; i++) {
        ctrlImages[i] = new Image();
        ctrlImages[i].src = preload.arguments[i];
    }
}

preload(
    " "
)


//override _drawControl function to change the corner images
fabric.Object.prototype._drawControl = function (control, ctx, methodName, left, top, flipiX, flipiY) {

    var sizeX = this.cornerSize / this.scaleX,
        sizeY = this.cornerSize / this.scaleY;

    if (this.isControlVisible(control)) {
               /* isVML ||*/ this.transparentCorners || ctx.clearRect(left, top, sizeX, sizeY);


        var SelectedIconImage = new Image();
        var lx = '';
        var ly = '';
        var n = '';

        switch (control) {
            case 'tl':
                if (flipiY) { ly = 'b'; } else { ly = 't'; }
                if (flipiX) { lx = 'r'; } else { lx = 'l'; }
                break;
            case 'tr':
                if (flipiY) { ly = 'b'; } else { ly = 't'; }
                if (flipiX) { lx = 'l'; } else { lx = 'r'; }
                break;
            case 'bl':
                if (flipiY) { ly = 't'; } else { ly = 'b'; }
                if (flipiX) { lx = 'r'; } else { lx = 'l'; }
                break;
            case 'br':
                if (flipiY) { ly = 't'; } else { ly = 'b'; }
                if (flipiX) { lx = 'l'; } else { lx = 'r'; }
                break;
            default:
                ly = control.substr(0, 1);
                lx = control.substr(1, 1);
                break;
        }

        control = ly + lx;

        switch (control) {
            case 'tl':
                SelectedIconImage.src = ctrlImages[1].src;
                break;
            case 'tr':
                if (flipiX && !flipiY) { n = '5'; }
                if (!flipiX && flipiY) { n = '3'; }
                if (flipiX && flipiY) { n = '4'; }
                SelectedIconImage.src = ctrlImages[0].src;
                break;
            case 'mt':

                break;
            case 'bl':
                if (flipiY) { n = '2'; }
                SelectedIconImage.src = ctrlImages[3].src;
                break;
            case 'br':
                if (flipiX || flipiY) { n = '2'; }
                if (flipiX && flipiY) { n = ''; }
                SelectedIconImage.src = ctrlImages[2].src;
                break;
            case 'mb':

                break;
            case 'ml':

                break;
            case 'mr':

                break;
            default:
                ctx[methodName](left, top, sizeX, sizeY);
                break;
        }

        if (control == 'tl' || control == 'tr' || control == 'bl' || control == 'br'
            || control == 'mt' || control == 'mb' || control == 'ml' || control == 'mr') {
            sizeX = 30;
            sizeY = 30;
            ctx.drawImage(SelectedIconImage, left, top, sizeX, sizeY);
        }


        try {
            ctx.drawImage(SelectedIconImage, left, top, sizeX, sizeY);

        } catch (e) {
            if (e.name != "NS_ERROR_NOT_AVAILABLE") {
                throw e;
            }
        }


    }
};//end

 





/****/

var readFile = function (e) {
    inputforupload = e.target;
    readerobj = new FileReader();

    readerobj.onload = function () {
        var imgElement = document.createElement('img');
        imgElement.src = readerobj.result;

        imgElement.onload = function () {


            console.log(imgElement.width);
            console.log(imgElement.height);

            
            var imageinstance = new fabric.Image(imgElement, {
                angle: 0,
                opacity: 1,
                cornerSize: 30,
            });
    
            var cw = $(".canvas-container").width();
            var ch = $(".canvas-container").height();
            if (cw > ch) {
                /** canvas ist landscape **/
                imageinstance.scaleToWidth($(".canvas-container").width() - 200);
                imageinstance.scaleToHeight($(".canvas-container").height() - 200);

            } else {
                /** canvas ist portrait **/
                imageinstance.scaleToHeight($(".canvas-container").height() - 200);
                imageinstance.scaleToWidth($(".canvas-container").width() - 200);

            }

            imageinstance.setControlsVisibility(HideControls);
            canvasObj.add(imageinstance);
            canvasObj.centerObject(imageinstance);


        };


    };

    readerobj.readAsDataURL(inputforupload.files[0]);
};

document.getElementById('filereader').addEventListener('change', readFile);




canvasObj.on('mouse:down', function (e) {
    if (canvasObj.getActiveObject()) {
        var target = this.findTarget();

        if (target.__corner == 'tr') {
            canvasObj.remove(canvasObj.getActiveObject());

        } else if (target.__corner == 'tl') {
            var curAngle = canvasObj.getActiveObject().get('angle');
            canvasObj.getActiveObject().set('angle', curAngle + 15);
            console.log('rotate pressed');
        } else if (target.__corner == 'bl') {
            console.log('scale pressed');
        } else if (target.__corner == 'br') {
            console.log('copy pressed');
        }
    }

});



