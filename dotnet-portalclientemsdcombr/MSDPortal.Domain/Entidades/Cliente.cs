﻿using MSDPortal.Domain.Entidades;

namespace MSDPortal.Application.Entidades
{
    public class Cliente : BaseEntity
    {
        public string Nome { get; set; }
        public string Apelido {  get; set; }
        public string Codigo { get; set; }

    }


}
