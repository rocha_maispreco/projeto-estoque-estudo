﻿using MSDPortal.Application;
using MSDPortal.Application.Entidades;
using MSDPortal.Domain.Interfaces.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MSDPortal.Presentation.Web.Controllers
{
    public class ClienteController : Controller
    {
        private readonly ClienteService _clienteService;

        public ClienteController(ClienteService clienteService)
        {
            _clienteService = clienteService;
        }

        // GET: Cliente
        public async Task<ActionResult> Index()
        {
            List<Cliente> listaClientes = await _clienteService.GetAll();
            return View(listaClientes);
        }

        // GET: Cliente/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Cliente/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Cliente/Create
        [HttpPost]
        public async Task<ActionResult> Create(Cliente model)
        {
            try
            {
                await _clienteService.Add(model);
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Cliente/Edit/5
        public async Task<ActionResult> Edit(int id)
        {

            Cliente cliente = await _clienteService.Get(id);
            return View(cliente);
        }

        // POST: Cliente/Edit/5
        [HttpPost]
        public async Task<ActionResult> Edit(int id, Cliente model)
        {
            try
            {
                await _clienteService.Update(model);

                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Cliente/Delete/5
        public ActionResult Delete(int id)
        {

            return View(new Cliente());
        }

        // POST: Cliente/Delete/5
        [HttpPost]
        public async Task<ActionResult> Delete(int id, Cliente model)
        {
            try
            {
                // TODO: Add delete logic here
                _clienteService.Delete(model);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
