﻿using AutoMapper;
using MSDPortal.Application.Entidades;
using MSDPortal.Domain.Interfaces;
using MSDPortal.Domain.Interfaces.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MSDPortal.Application
{
    public class ProdutoService
    {
        private readonly IUnitofWork _unitofWork;
        private readonly IMapper _mapper;
        private readonly IProdutoRepository _produtoRepository;

        public ProdutoService(IUnitofWork unitofWork, IProdutoRepository produtoRepository)
        {
            _unitofWork = unitofWork;
            _produtoRepository = produtoRepository;
        }

        public async Task<Produto> Add(Produto model)
        {
            try
            {
                await _unitofWork.ProdutoRepository.Add(model);
            }
            catch (Exception ex)
            {
                var error = ex.Message;
                throw ex;
            }

            return model;
        }

        public void Delete(Produto model)
        {
            try
            {

                _unitofWork.ProdutoRepository.Remove(model);
            }
            catch (Exception ex)
            {
                var error = ex.Message;
                throw ex;
            }
        }

        public async Task<Produto> Get(int id)
        {
            try
            {
                Produto model = await _unitofWork.ProdutoRepository.GetById(id);
                return model;
            }
            catch (Exception ex)
            {
                var error = ex.Message;
                throw ex;
            }
        }

        public async Task<List<Produto>> GetAll()
        {

            try
            {
                IEnumerable<Produto> result = await _unitofWork.ProdutoRepository.GetAllAsync();
                return result.ToList();
            }
            catch (Exception ex)
            {
                var error = ex.Message;
                throw ex;
            }
        }

        public async Task<Produto> Update(Produto model)
        {
            try
            {
                Produto result = await _unitofWork.ProdutoRepository.Update(model);
                return result;

            }
            catch (Exception ex)
            {
                var error = ex.Message;
                throw ex;
            }

        }
    }
}
