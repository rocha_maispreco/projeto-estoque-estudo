﻿using AutoMapper;

namespace MSDPortal.Application.Mappers
{
    public class AutoMapperConfig
    {

        //public override void Load()
        //{
        //    Bind<IValueResolver<SourceEntity, DestModel, bool>>().To<MyResolver>();

        //    var mapperConfiguration = CreateConfiguration();
        //    Bind<MapperConfiguration>().ToConstant(mapperConfiguration).InSingletonScope();

        //    // This teaches Ninject how to create automapper instances say if for instance
        //    // MyResolver has a constructor with a parameter that needs to be injected
        //    Bind<IMapper>().ToMethod(ctx =>
        //         new Mapper(mapperConfiguration, type => ctx.Kernel.Get(type)));
        //}

        public static MapperConfiguration CreateConfiguration()
        {
            var configuration = new MapperConfiguration(cfg =>
            {

            });

            return configuration;
        }
    }
}
