﻿using MSDPortal.Application.Entidades;
using MSDPortal.Data.Context;
using MSDPortal.Domain.Interfaces.Repository;

namespace MSDPortal.Data.Repository
{
    public class ClienteRepository : BaseRepository<Cliente>, IClienteRepository
    {
        public ClienteRepository(MSDPortalContext context) : base(context)
        {
        }
    }
}
